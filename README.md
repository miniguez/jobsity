DIRECTORY STRUCTURE
------------
app/           contains app
Database/      contains model and dump for blog database
VirtualHost/   contains the configuration of the host 


REQUIREMENTS
------------
The minimum requirement by this project template that your Web server supports PHP 7.3.0, MySQL 5.7
You need to have enable mod_rewrite in php (https://do.co/2X2QUpt) and apache2

INSTALLATION
------------
Follow the steps:
 1. git clone https://gitlab.com/miniguez/jobsity.git
 2. cd jobsity/app/
 3. composer install  
 4. create the databases blog and blog_test 
 5. load dump Database/blog_dump.sql (mysql -u root blog < blog_dump.sql)
 6. enter  [http://localhost/jobsity/app/web/](http://localhost/jobsity/app/web/entries/index)
 
 **Note**: The app has two users: admin and miniguez, both with the password admin123

CONFIG SENDMAIL 
------------
To use the features to send a confirmation account and restore password. you have to config the lines 
'username' => 'your_gmail_account'
'password' => 'your_password'

in the file app/config/web.php

Note: by default the app use smtp.gmail.com you can change it if you want.

RUN UNIT TEST
------------

 1. cd app/
 2. php vendor/bin/codecept run -v unit


SET VIRTUALHOST
------------
1. sudo cp VirtualHost/miniguez.jobsitychallenge.com.conf   /etc/apache2/sites-available/
2. sudo a2ensite miniguez.jobsitychallenge.com.conf
3. sudo systemctl restart apache2
