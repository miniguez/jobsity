<?php 
use yii\widgets\ListView;
use yii\helpers\Html;


echo "<div align=\"right\">".Html::a('New post', ['/entries/create'], ['class'=>'btn btn-primary'])."</div>";
?>
<div class="list-group">
<?php
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_entry',
]);
?>
</div>