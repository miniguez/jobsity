<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Entries */

$this->title = 'Update Entries: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Entries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="entries-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
