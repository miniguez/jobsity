<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>

<div class="list-group-item list-group-item-action flex-column align-items-start">    
  <h5 class="mb-1"><?= Html::encode($model->title) ?></h5>              
  <p class="mb-1"><?= HtmlPurifier::process($model->content) ?></p>    
  <small><?= Html::encode($model->creation_date) ?></small>
  
<?php 
if (isset($_SESSION["__id"]) && $model->author == $_SESSION["__id"]){
  echo Html::a('edit entry', ['/entries/update', 'id'=>$model->id], ['class'=>'glyphicon glyphicon-edit']);

}
?>
</div>
