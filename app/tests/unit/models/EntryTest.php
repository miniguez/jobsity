<?php

namespace tests\unit\models;
use app\models\Entries;

class EntryTest extends \Codeception\Test\Unit
{
    protected $arr_valid_data = [
        'creation_date'=>'2019-11-09 19:03:03', 
        'title'=>'Codeception test', 
        'content'=>'Testing by codeception unit',
        'author'=>1
    ];
    protected $arr_invalid_data = [
        'creation_date'=>'2019-11-09 19:03:03', 
        'title'=>'', 
        'content'=>'Testing by codeception unit',
        'author'=>999
    ];

    public function testCreateEntry()
    {                
        $entry= $this->saveEntry($this->arr_valid_data);        
        expect($entry->title)->equals('Codeception test');
    }

    public function testErrorCreateEntry()
    {
        $entry= $this->saveEntry($this->arr_invalid_data);        
        expect($entry['title'][0])->equals("Title cannot be blank.");
        expect($entry['author'][0])->equals("Author is invalid.");
    }

    public function testEditEntry()
    {
        $entry = Entries::findOne(1);
        $entry->title = "Some test to update title";
        $entry->save();
        expect($entry->title)->equals("Some test to update title");
    }
    /**
     * Function Helper
     * To: save an entry
     */
    protected function saveEntry($data)
    {
        $entry = new Entries();
        $entry->creation_date = $data['creation_date'];
        $entry->title = $data['title'];
        $entry->content = $data['content'];
        $entry->author = $data['author'];
        
        return ($entry->save()) ? $entry : $entry->getErrors(); 
    }
}