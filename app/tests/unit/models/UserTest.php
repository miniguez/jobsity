<?php

namespace tests\unit\models;

use dektrium\user\models\User;

class UserTest extends \Codeception\Test\Unit
{
    protected $arr_valid_data = [
        'username'=>'admin2', 
        'email'=>'email@mail.com', 
        'password_hash'=>'password_hash',
        'auth_key'=>'tfFTPqD3Xrf-GiILsv-SzeUfwV0GN3tG',
        'created_at' => 1573331461,
        'updated_at'=> 1573331461,
        'flags' => 0
    ];
    protected $arr_invalid_data = [
        'username'=>'admin', 
        'email'=>'email@mail.com', 
        'password_hash'=>'password_hash',
        'auth_key'=>'tfFTPqD3Xrf-GiILsv-SzeUfwV0GN3tG',
        'created_at' => 1573331461,
        'updated_at'=> 1573331461,
        'flags' => 0
    ];
    public function testCreateUser()
    {                
        $user= $this->saveUser($this->arr_valid_data);        
        expect($user->username)->equals('admin2');
    }

    public function testFindUser()
    {        
        expect_that(User::find(1));  
        expect_not(User::findIdentity(999));
    }

    public function testErrorUser()
    {
        $user = $this->saveUser($this->arr_invalid_data);
        expect($user['username'][0])->equals("This username has already been taken");
    }

    /**
     * Function Helper
     * To: save an user
     */
    protected function saveUser($data)
    {
        $user = new User();
        $user->username = $data['username'];
        $user->email = $data['email'];
        $user->password_hash = $data['password_hash'];
        $user->auth_key = $data['auth_key'];
        $user->created_at = $data['created_at'];
        $user->updated_at= $data['updated_at'];
        $user->flags = $data['flags'];
        return ($user->save()) ? $user : $user->getErrors(); 
    }
}
