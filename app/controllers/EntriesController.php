<?php

namespace app\controllers;

use Yii;
use app\models\Entries;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\helpers\Url;
use yii\data\ActiveDataProvider;

/**
 * EntriesController implements the CRUD actions for Entries model.
 */
class EntriesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Entries models.
     * @return mixed
     */
    public function actionIndex()
    {
        $entries = Entries::find()
        ->orderBy(['(creation_date)' => SORT_DESC]);     
        
        $dataProvider = new ActiveDataProvider([
            'query' => $entries,
            'pagination' => [
                'pageSize' => 3,
            ],
        ]);

        return $this->render('index', [            
            'dataProvider' => $dataProvider            
        ]);
    }

    /**
     * Displays a single Entries model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Entries model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['user/login']), 302);
        }
        $model = new Entries();
        
        if ($model->load(Yii::$app->request->post())) {
            $model->creation_date = date("Y-m-d H:i:s");
            $model->author = $_SESSION["__id"];
            if ($model->save()) {
                Yii::$app->session->setFlash('info', 'Post created!');
                return $this->redirect(['index']);
            }
            Yii::$app->session->setFlash('error', 'Oops something goes wrong!');            
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Entries model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
               Yii::$app->session->setFlash('info', 'Post updated!');
                return $this->redirect(['index']);
            }

            Yii::$app->session->setFlash('error', 'Oops something goes wrong!');
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Entries model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Entries model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Entries the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Entries::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
