<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use dektrium\user\models\Profile;
use dektrium\user\models\User;
use app\models\Entries;

class ProfileController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Show user profile
     * @return mixed
     */
    public function actionShow($id)
    {
        $profile = $this->findModel($id);
        
        $user = User::findOne($profile->user_id);

        $repositories = [];         
        try{
            $client = new \Github\Client();
            $repositories = $client->api('user')->repositories($user->username); 
        } catch (\Exception $e) {
            echo $e->getMessage(), "\n";
        }
                     

        $entries = Entries::find()
        ->where('author=:author')
        ->addParams([':author' => $profile->user_id])
        ->orderBy(['(creation_date)' => SORT_DESC]);     
        
        $dataProvider = new ActiveDataProvider([
            'query' => $entries,
            'pagination' => [
                'pageSize' => 3,
            ],
        ]);

        return $this->render('show', [
            'profile' => $profile,
            'dataProvider' => $dataProvider,
            'repositories' => $repositories
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Profile::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}