<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entries".
 *
 * @property int $id
 * @property string $creation_date
 * @property string $title
 * @property string $content
 * @property int $author
 *
 * @property User $author0
 */
class Entries extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'entries';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['creation_date', 'title', 'content', 'author'], 'required'],
            [['creation_date'], 'safe'],
            [['content'], 'string'],
            [['author'], 'integer'],
            [['title'], 'string', 'max' => 85],
            [['author'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'creation_date' => 'Creation Date',
            'title' => 'Title',
            'content' => 'Content',
            'author' => 'Author',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor0()
    {
        return $this->hasOne(User::className(), ['id' => 'author']);
    }
}
