<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%entries}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 */
class m191110_032307_create_entries_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%entries}}', [
            'id' => $this->primaryKey(),
            'creation_date' => $this->dateTime(),
            'title' => $this->string(85),
            'content' => $this->text(),
            'author' => $this->integer()->notNull(),
        ]);

        // creates index for column `author`
        $this->createIndex(
            '{{%idx-entries-author}}',
            '{{%entries}}',
            'author'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-entries-author}}',
            '{{%entries}}',
            'author',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-entries-author}}',
            '{{%entries}}'
        );

        // drops index for column `author`
        $this->dropIndex(
            '{{%idx-entries-author}}',
            '{{%entries}}'
        );

        $this->dropTable('{{%entries}}');
    }
}
